using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCoche : MonoBehaviour
{

    void Update()
    {
        Vector3 pos = transform.position;
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            pos = pos - transform.forward;
            if (pos.y < 1.8f)
            {
                transform.position = pos;
            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            pos = pos + transform.forward;
            if (pos.y > 1f)
            {
                transform.position = pos;
            }
        }
    }
}
