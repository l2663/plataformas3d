using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightChange : MonoBehaviour
{
    Light lt;
    bool change = false;

    void Start()
    {
        this.lt = this.GetComponent<Light>();
        StartCoroutine(ChangeColor());
    }

    void Update()
    {
        if (this.transform.localEulerAngles.x > 90)
        {
            change = true;
        }
        if (this.transform.localEulerAngles.x < -30)
        {
            change = false;
        }

        if (!change)
        {
            this.transform.Rotate(new Vector3(0.01f, 0, 0));
        }
        else
        {
            this.transform.Rotate(new Vector3(-0.01f, 0, 0));
        }
    }

    IEnumerator ChangeColor()
    {
        yield return new WaitForSeconds(5);

        int x = Random.Range(0, 3);
        if (x == 0)
        {
            this.lt.color = new Color(Random.Range(0, 255), 255, 255);
        }
        else if (x == 1)
        {
            this.lt.color = new Color(255, Random.Range(0, 255), 255);
        }
        else if (x == 2)
        {
            this.lt.color = new Color(255, 255, Random.Range(0, 255));
        }
    }

}
