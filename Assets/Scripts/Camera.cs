using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public Transform target;
    private Vector3 targetPos;

    void Update()
    {
        targetPos = target.position;

        RaycastHit hit;
        if (Physics.Linecast(target.parent.position, target.position, out hit))
        {
            targetPos = hit.point;
            print(hit.distance);
            if (hit.distance < 1.5f)
            {

            }
            else
            {

            }

        }
        else
        {

        }
        CameraLerp(targetPos);

        if (Input.GetMouseButton(0))
        {
            this.transform.localEulerAngles = new Vector3(this.transform.localEulerAngles.x, -180, this.transform.localEulerAngles.z);
        }
        else
        {
            this.transform.localEulerAngles = new Vector3(this.transform.localEulerAngles.x, 0, this.transform.localEulerAngles.z);
        }

    }

    private void CameraLerp(Vector3 targetPos)
    {
        this.transform.position = Vector3.Lerp(this.transform.position, targetPos, Time.deltaTime*3);
        this.transform.LookAt(target.parent);

    }

}
