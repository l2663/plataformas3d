using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelota : MonoBehaviour
{
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        this.rb = GetComponent<Rigidbody>();
        this.rb.velocity = -transform.forward * 5;
        this.rb.AddTorque(this.rb.velocity * 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Pollo")
        {
            collision.gameObject.GetComponent<Pollo>().Die();
        }
    }

}
