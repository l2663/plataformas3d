using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Coche : MonoBehaviour
{
    public Transform init;
    public List<CocheInfo> cocheInfo;
    public float maxMotorTorque;
    public float maxSteeringAngle;

    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);
        if (collider.transform.tag == "Left")
        {
            rotation = rotation * Quaternion.Euler(new Vector3(-90, -180, 0));
        }
        else
        {
            rotation = rotation * Quaternion.Euler(new Vector3(-90, 0, 0));
        }
        visualWheel.transform.rotation = rotation;
        visualWheel.transform.position = position;

    }

    public void FixedUpdate()
    {
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (CocheInfo cocheInfo in cocheInfo)
        {
            if (cocheInfo.steering)
            {
                cocheInfo.leftWheel.steerAngle = steering;
                cocheInfo.rightWheel.steerAngle = steering;
            }
            if (cocheInfo.motor)
            {
                cocheInfo.leftWheel.motorTorque = motor;
                cocheInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(cocheInfo.leftWheel);
            ApplyLocalPositionToVisuals(cocheInfo.rightWheel);
        }
    }
    
    public void Win()
    {
        SceneManager.LoadScene("Win");
    }

    public void Die()
    {
        print("iker");
        this.transform.position = init.position;
    }

}
