using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class Pollo : MonoBehaviour
{
    public Camera cam;
    private float lookSpeed = 2;

    private Animator anim;
    private Rigidbody rb;
    private State state;
    private float spd = 4;

    private Vector2 value;
    private bool move = false;
    private bool shift = false;

    private string idle = "Idle";
    private string walk = "Walk In Place";
    private string run = "Run In Place";

    private Transform checkpoint;

    private void Awake()
    {
        this.anim = this.GetComponent<Animator>();
        this.rb = this.GetComponent<Rigidbody>();
        this.state = State.IDLE;
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        CameraControl();

        if (this.move)
        {
            this.Movement();
        }
    }

    private void CameraControl()
    {
        transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Suelo")
        {
            switch (state)
            {
                case State.JUMP:
                    if (this.move)
                    {
                        if (shift)
                        {
                            this.ChangeState(State.WALK, walk);
                        }
                        else
                        {
                            this.ChangeState(State.RUN, run);
                        }
                    }
                    else
                    {
                        ChangeState(State.IDLE, idle);
                    }
                    break;
            }
        }
        else if (collision.transform.tag == "Movil")
        {
            this.transform.parent = collision.gameObject.transform;

            switch (state)
            {
                case State.JUMP:
                    if (this.move)
                    {
                        if (shift)
                        {
                            this.ChangeState(State.WALK, walk);
                        }
                        else
                        {
                            this.ChangeState(State.RUN, run);
                        }
                    }
                    else
                    {
                        ChangeState(State.IDLE, idle);
                    }
                    break;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Movil")
        {
            this.transform.parent = null;
        }
    }

    public void Movement()
    {
        this.rb.velocity = new Vector3(0,this.rb.velocity.y,0);

        if (this.value.x < -0.5 || this.value.x > 0.5)
        {
            this.rb.velocity += this.transform.right * (this.value.x * spd/2);
        }

        if (this.value.y > 0.5 || this.value.y < 0.5)
        {
            if (this.state == State.WALK || this.state == State.JUMP)
            {
                this.rb.velocity += this.transform.forward * (this.value.y * (spd / 2));
            }
            else
            {
                this.rb.velocity += this.transform.forward * (this.value.y * spd);
            }

        }
        
    }

    public void Move(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            this.move = true;
            this.value = context.ReadValue<Vector2>();
            switch (state)
            {
                case State.IDLE:
                    if (shift)
                    {
                        ChangeState(State.WALK, walk);
                    }
                    else
                    {
                        ChangeState(State.RUN, run);
                    }
                    break;
            }
        }
        if (context.canceled)
        {
            this.move = false;
            switch (state)
            {
                case State.WALK:
                    ChangeState(State.IDLE, idle);
                    break;
                case State.RUN:
                    ChangeState(State.IDLE, idle);
                    break;
            }
        }
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (state != State.JUMP)
            {
                this.rb.AddForce(0, 6, 0, ForceMode.Impulse);
                this.ChangeState(State.JUMP, idle);
            }
        }
    }

    public void Shift(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            this.shift = true;
            switch (state)
            {
                case State.RUN:
                    ChangeState(State.WALK, walk);
                    break;
            }
        }
        if (context.canceled)
        {
            this.shift = false;
            switch (state)
            {
                case State.WALK:
                    ChangeState(State.RUN, run);
                    break;
            }
        }
    }

    public void Checkpoint(Transform check)
    {
        this.checkpoint = check;
        print("Checkpoint: " + this.checkpoint.position);
    }

    public void Die()
    {
        this.rb.velocity = Vector3.zero;
        this.ChangeState(State.IDLE, idle);
        if (this.checkpoint != null)
        {
            this.transform.position = new Vector3(this.checkpoint.position.x, this.checkpoint.position.y + 10, this.checkpoint.position.z);
        }
        else
        {
            this.transform.position = new Vector3(0, 10, 0);
        }
    }

    private void ChangeState(State state, string anim)
    {
        if (this.state != state)
        {
            this.state = state;
            this.anim.Play(anim);
        }
    }

}

public enum State
{
    IDLE,
    WALK,
    RUN,
    JUMP
}
