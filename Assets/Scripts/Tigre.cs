using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;

public class Tigre : MonoBehaviour
{
    private Transform init;
    private Rigidbody rb;
    private Animator anim;
    private TigerState state;
    private string idle;
    private string walk;
    private string run;
    private bool patrullar = true;
    private bool perseguir = false;
    private bool change = false;
    private int spd = 4;
    public Transform punto1;
    public Transform punto2;
    private GameObject focus;

    private void Awake()
    {
        this.init = this.GetComponent<Transform>();
        this.rb = this.GetComponent<Rigidbody>();
        this.anim = this.GetComponent<Animator>();
        this.idle = "idle";
        this.walk = "walk";
        this.run = "run";
    }

    void Start()
    {
        this.ChangeState(TigerState.WALK, walk);
    }

    
    void Update()
    {
        if (patrullar)
        {
            this.Patrullar();
        }
        if (perseguir)
        {
            this.Perseguir();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Punto"))
        {
            if (this.patrullar)
            {
                if (this.change)
                {
                    this.change = false;
                }
                else
                {
                    this.change = true;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Pollo"))
        {
            this.perseguir = false;
            this.ChangeState(TigerState.WALK, walk);
            this.Restart();
        }
    }

    private void Patrullar()
    {
        if (!change)
        {
            transform.LookAt(punto2.transform.position);
        }
        else
        {
            transform.LookAt(punto1.transform.position);
        }
        this.rb.velocity = this.transform.forward * spd;

        RaycastHit info;
        Debug.DrawLine(this.transform.Find("Ray").position, this.transform.forward * 30, Color.red);
        if (Physics.Raycast(this.transform.Find("Ray").position, this.transform.forward, out info, 30))
        {
            //print("RAYCAST HIT " + info.distance + " " + info.transform.tag);
            if (info.transform.tag == "Pollo")
            {
                this.patrullar = false;
                this.perseguir = true;
                this.focus = info.transform.gameObject;
                this.ChangeState(TigerState.RUN, run);
            }
        }
    }

    private void Perseguir()
    {
        transform.LookAt(focus.transform.position);
        this.rb.velocity = this.transform.forward * (spd * 3);
    }

    private void Restart()
    {
        transform.LookAt(init.transform.position);
        this.rb.velocity = this.transform.forward * spd;
        if (this.transform.position == init.transform.position)
        {
            this.patrullar = true;
        }
    }

    private void ChangeState(TigerState state, string s)
    {
        this.state = state;
        this.anim.Play(s);
    }

}

public enum TigerState
{
    IDLE,
    WALK,
    RUN
}