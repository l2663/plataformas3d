using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendu : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(PARAFOCA());
    }

    void Update()
    {
        
    }

    IEnumerator PARAFOCA()
    {
        yield return new WaitForSeconds(1);
        var motor = this.GetComponent<HingeJoint>().motor;
        motor.force = 0;
        this.GetComponent<HingeJoint>().motor = motor;
    }

}
